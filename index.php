<?php 

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Nama : ". $sheep->name ."<br>"; // "shaun"
echo "legs : ". $sheep->legs ."<br>"; // 4
echo "cold : ". $sheep->cold_blooded ."<br><br>"; // "no"

$frog = new Frog("buduk");
echo "Nama : ". $frog->name ."<br>";
echo "legs : ". $frog->legs ."<br>"; // 4
echo "cold : ". $frog->cold_blooded ."<br>";
echo "Jump : ". $frog->jump ."<br><br>"; // "no"

$ape = new Ape("Kera Sakti");
echo "Nama : ". $ape->name ."<br>";
echo "legs : ". $ape->legs ."<br>"; // 4
echo "cold : ". $ape->cold_blooded ."<br>";
echo "Yell: ". $ape->yell ."<br>"; // "no"
?>